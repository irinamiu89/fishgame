var app = new PIXI.Application(1552, 768, { antialias: true });
document.body.appendChild(app.view);

var bg = PIXI.Sprite.fromImage('images/bg_landscape.jpg');

app.stage.addChild(bg);

app.stage.interactive = true;

var container = new PIXI.Container();
app.stage.addChild(container);

var fishArray = [];

PIXI.loader
    .add('images/sprites/fish1.json')
    .add('images/sprites/fish1_turn.json')
    .add('images/sprites/fish1_turn2.json')
    .add('images/sprites/fish2.json')
    .add('images/sprites/fish2_turn.json')
    .add('images/sprites/fish2_turn2.json')
    .add('images/sprites/fish3.json')
    .add('images/sprites/fish3_turn.json')
    .add('images/sprites/fish3_turn2.json')
    .add('images/sprites/fish4.json')
    .add('images/sprites/fish4_turn.json')
    .add('images/sprites/fish4_turn2.json')
    .add('images/sprites/fish5.json')
    .add('images/sprites/fish5_turn.json')
    .add('images/sprites/fish5_turn2.json')
    .add('images/sprites/bubbles_anim.json')
    .load(onAssetsLoaded);    
    
  
    function onAssetsLoaded(){
      
      var bubbles = [];
      
      for (var j = 1; j < 4; j++){
        var value = j < 10 ? '0' + j : j;
        bubbles.push(PIXI.Texture.fromFrame('bubbles_' + value + '.png'));
      }
      for (i =1; i<4; i++ ){
      var bubbles_anim = new PIXI.extras.AnimatedSprite(bubbles);
      
        bubbles_anim.x = 1300;
        bubbles_anim.y = 300;
        bubbles_anim.anchor.set(0.5);
        bubbles_anim.animationSpeed = 0.03;
        bubbles_anim.scale.x = 0.5;
        bubbles_anim.scale.y = 0.5;
        bubbles_anim.play();
        bubbles_anim.loop = true;
        bubbles_anim.visible = true;
        bubbles_anim.buttonMode = true;
        bubbles_anim.on('pointerover', onClickAlgae_3_left);
        app.stage.addChild(bubbles_anim);}
        
       var algae_3_left =  PIXI.Sprite.fromImage('images/algae_3_left.png');
        algae_3_left.anchor.set(0.5);
        algae_3_left.interactive = true;
        algae_3_left.buttonMode = true;
        algae_3_left.on('pointerover', onClickAlgae_3_left);
        app.stage.addChild(algae_3_left);
        
        function onClickAlgae_3_left () {
          bubble_single.visible = true;
          bubble_single.x = algae_3_left.x;
          bubble_single.y = algae_3_left.y;
          bubble_single.scale.x = bubble_single.scale.y = 0.05;
          bubbles_anim.x = 300;
          bubbles_anim.y = 300;
          bubbles_anim.anchor.set(0.5);
          bubbles_anim.animationSpeed = 0.03;
          bubbles_anim.scale.x = 0.5;
          bubbles_anim.scale.y = 0.5;
          bubbles_anim.play();
          bubbles_anim.loop = true;
        }
        
      var fish1_turn = [];

        for (var i = 1; i <= 24; i++) {
            var val = i < 10 ? '0' + i : i;

            fish1_turn.push(PIXI.Texture.fromFrame('Fish1a_' + val + '.png'));
        }
      
        var fish1_turn_anim = new PIXI.extras.AnimatedSprite(fish1_turn);        

        fish1_turn_anim.x = 900;
        fish1_turn_anim.y = 400;        
        fish1_turn_anim.scale.x = 0.2;
        fish1_turn_anim.scale.y = 0.2;
        fish1_turn_anim.pivot.set(0.5, 0.5);
        fish1_turn_anim.animationSpeed = 0.3;
        fish1_turn_anim.interactive = true;
        fish1_turn_anim.buttonMode = true;
        fish1_turn_anim.direction = Math.random() * Math.PI * 2;
        fish1_turn_anim.turningSpeed = Math.random() - 1;
        fish1_turn_anim.speed = 1 + Math.random() * 1.1;
        fish1_turn_anim.x += Math.sin(count) * fish1_turn_anim.animationSpeed;
        fish1_turn_anim.y += Math.cos(count) * fish1_turn_anim.animationSpeed;
        fish1_turn_anim.play();
        fish1_turn_anim.loop = false;
        fish1_turn_anim.on('pointerdover', onClickFish1_turn);
        app.stage.addChild(fish1_turn_anim);
      
       
        
      var bubble_single = PIXI.Sprite.fromImage('images/bubble_single.png');
       bubble_single.x = fish1_turn_anim.x ;
       bubble_single.y = fish1_turn_anim.y ;
       bubble_single.scale.x = bubble_single.scale.y = 0.2;
       bubble_single.visible = false;
       app.stage.addChild(bubble_single);
        
        function onClickFish1_turn () {
         fish1_turn_anim.scale.x +=  0.3;
          fish1_turn_anim.scale.y +=  0.3;
          bubble_single.visible = true;
          bubble_single.alpha = 1;
          
        }
      
        
      var fish1_turn2 = [];

        for (var ii = 1; ii <= 24; ii++) {
            var val = ii < 10 ? '0' + ii : ii;

            fish1_turn2.push(PIXI.Texture.fromFrame('Fish1b_' + val + '.png'));
        }
      
        var fish1_turn2_anim = new PIXI.extras.AnimatedSprite(fish1_turn2);        

        fish1_turn2_anim.x = Math.floor(Math.random() * app.renderer.width);
        fish1_turn2_anim.y = 500;        
        fish1_turn2_anim.scale.set(0.15 + Math.random() * 0.2);
        fish1_turn2_anim.pivot.set(0.5, 0.5);
        fish1_turn2_anim.animationSpeed = 0.3;
        fish1_turn2_anim.direction = Math.random() * Math.PI * 2;
        fish1_turn2_anim.turningSpeed = Math.random() - 1;
        fish1_turn2_anim.speed = 1 + Math.random() * 1.1;
        fish1_turn2_anim.x += Math.sin(count) * fish1_turn2_anim.animationSpeed;
        fish1_turn2_anim.y += Math.cos(count) * fish1_turn2_anim.animationSpeed;
        fish1_turn2_anim.play();
        fish1_turn2_anim.loop = false;
        app.stage.addChild(fish1_turn2_anim);
        
        var fish2_turn = [];

        for (var i = 1; i <= 24; i++) {
            var val = i < 10 ? '0' + i : i;

            fish2_turn.push(PIXI.Texture.fromFrame('Fish2a_' + val + '.png'));
        }
      
        var fish2_turn_anim = new PIXI.extras.AnimatedSprite(fish2_turn);        

        fish2_turn_anim.x = 900;
        fish2_turn_anim.y = 200;        
        fish2_turn_anim.scale.x = 0.2;
        fish2_turn_anim.scale.y = 0.2;
        fish2_turn_anim.pivot.set(0.5, 0.5);
        fish2_turn_anim.animationSpeed = 0.3;
        fish2_turn_anim.visible = false;
        //fish2_turn_anim.interactive = true;
        //fish2_turn_anim.buttonMode = true;
        fish2_turn_anim.direction = Math.random() * Math.PI * 2;
        fish2_turn_anim.turningSpeed = Math.random() - 1;
        fish2_turn_anim.speed = 1 + Math.random() * 1.1;
        fish2_turn_anim.x += Math.sin(count) * fish2_turn_anim.animationSpeed;
        fish2_turn_anim.y += Math.cos(count) * fish2_turn_anim.animationSpeed;
        //fish2_turn_anim.play();
        fish2_turn_anim.loop = false;
        //fish2_turn_anim.on('pointerdown', onClickFish2_turn);
        app.stage.addChild(fish2_turn_anim);
        setTimeout (function (){
          fish2_turn_anim.visible = true;
          fish2_turn_anim.play();
        },2000);
        
        var fish2_turn2 = [];

        for (var i = 1; i <= 24; i++) {
            var val = i < 10 ? '0' + i : i;

            fish2_turn2.push(PIXI.Texture.fromFrame('Fish2b_' + val + '.png'));
        }
      
        var fish2_turn2_anim = new PIXI.extras.AnimatedSprite(fish2_turn2);        

        fish2_turn2_anim.x = 1200;
        fish2_turn2_anim.y = 300;        
        fish2_turn2_anim.scale.x = 0.2;
        fish2_turn2_anim.scale.y = 0.2;
        fish2_turn2_anim.pivot.set(0.5, 0.5);
        fish2_turn2_anim.animationSpeed = 0.3;
        fish2_turn2_anim.visible = false;
        //fish2_turn_anim.interactive = true;
        //fish2_turn_anim.buttonMode = true;
        fish2_turn2_anim.direction = Math.random() * Math.PI * 2;
        fish2_turn2_anim.turningSpeed = Math.random() - 1;
        fish2_turn2_anim.speed = 1 + Math.random() * 1.1;
        fish2_turn2_anim.x += Math.sin(count) * fish2_turn2_anim.animationSpeed;
        fish2_turn2_anim.y += Math.cos(count) * fish2_turn2_anim.animationSpeed;
        //fish2_turn_anim.play();
        fish2_turn2_anim.loop = false;
        //fish2_turn_anim.on('pointerdown', onClickFish2_turn);
        app.stage.addChild(fish2_turn2_anim);
        setTimeout (function (){
          fish2_turn2_anim.visible = true;
          fish2_turn2_anim.play();
        },3000);
        
      var fish3_turn = [];

        for (var i = 1; i <= 24; i++) {
            var val = i < 10 ? '0' + i : i;

            fish3_turn.push(PIXI.Texture.fromFrame('Fish3a_' + val + '.png'));
        }
      
        var fish3_turn_anim = new PIXI.extras.AnimatedSprite(fish3_turn);        

        fish3_turn_anim.x = 1600;
        fish3_turn_anim.y = 400;        
        fish3_turn_anim.scale.x = 0.2;
        fish3_turn_anim.scale.y = 0.2;
        fish3_turn_anim.pivot.set(0.5, 0.5);
        fish3_turn_anim.animationSpeed = 0.3;
        fish3_turn_anim.visible = false;
        //fish2_turn_anim.interactive = true;
        //fish2_turn_anim.buttonMode = true;
        fish3_turn_anim.direction = Math.random() * Math.PI * 2;
        fish3_turn_anim.turningSpeed = Math.random() - 1;
        fish3_turn_anim.speed = 1 + Math.random() * 1.1;
        fish3_turn_anim.x += Math.sin(count) * fish3_turn_anim.animationSpeed;
        fish3_turn_anim.y += Math.cos(count) * fish3_turn_anim.animationSpeed;
        //fish2_turn_anim.play();
        fish3_turn_anim.loop = false;
        //fish2_turn_anim.on('pointerdown', onClickFish2_turn);
        app.stage.addChild(fish3_turn_anim);
        setTimeout (function (){
          fish3_turn_anim.visible = true;
          fish3_turn_anim.play();
        },1000);
        
        var fish3_turn2 = [];

        for (var i = 1; i <= 24; i++) {
            var val = i < 10 ? '0' + i : i;

            fish3_turn2.push(PIXI.Texture.fromFrame('Fish3b_' + val + '.png'));
        }
      
        var fish3_turn2_anim = new PIXI.extras.AnimatedSprite(fish3_turn2);        

        fish3_turn2_anim.x = 1300;
        fish3_turn2_anim.y = 500;        
        fish3_turn2_anim.scale.x = 0.25;
        fish3_turn2_anim.scale.y = 0.25;
        fish3_turn2_anim.pivot.set(0.5, 0.5);
        fish3_turn2_anim.animationSpeed = 0.3;
        fish3_turn2_anim.visible = true;
        fish3_turn2_anim.direction = Math.random() * Math.PI * 2;
        fish3_turn2_anim.turningSpeed = Math.random() - 1;
        fish3_turn2_anim.speed = 1 + Math.random() * 1.1;
        fish3_turn2_anim.x += Math.sin(count) * fish3_turn2_anim.animationSpeed;
        fish3_turn2_anim.y += Math.cos(count) * fish3_turn2_anim.animationSpeed;
        fish3_turn2_anim.loop = false;
        app.stage.addChild(fish3_turn2_anim);
        setTimeout (function (){
          fish3_turn2_anim.visible = true;
          fish3_turn2_anim.play();
        },1000);
        
      var fish4_complete = [];
      
      for (var j = 1; j <= 24; j++){
        var value1 = j < 10 ? '0' + j : j;
        fish4_complete.push(PIXI.Texture.fromFrame('Fish4_' + value1 + '.png'));
      }
      
      var fish4_complete_anim = new PIXI.extras.AnimatedSprite(fish4_complete);
      
        fish4_complete_anim.x = 200;
        fish4_complete_anim.y = 400;        
        fish4_complete_anim.scale.x = fish4_complete_anim.scale.y = 0.3;
        fish4_complete_anim.pivot.set(0.5, 0.5);
        fish4_complete_anim.animationSpeed = 0.3; 
        fish4_complete_anim.direction = Math.random() * Math.PI * 2;
        fish4_complete_anim.turningSpeed = Math.random() - 1;
        fish4_complete_anim.speed = 1 + Math.random() * 1.1;
        fish4_complete_anim.x += Math.sin(count) * fish4_complete_anim.animationSpeed;
        fish4_complete_anim.y += Math.cos(count) * fish4_complete_anim.animationSpeed;
        fish4_complete_anim.play();
        fish4_complete_anim.interactive = true;
        fish4_complete_anim.buttonMode = true;
        fish4_complete_anim.on('pointerdown', onClickFish4_complete);
        fish4_complete_anim.loop = true;
        app.stage.addChild(fish4_complete_anim);
        fishArray.push(fish4_complete_anim);
        
        function onClickFish4_complete () {
          fish4_turn_anim.scale.x = fish4_turn_anim.scale.y = 0.3;          
          fish4_turn_anim.x = fish4_complete_anim.x;
          fish4_turn_anim.y = fish4_complete_anim.y;
          fish4_turn_anim.visible = true;
          //console.log("se intoarce");
          fish4_complete_anim.visible = false;
          fish4_complete_anim.alpha = 0;
          fish4_turn_anim.alpha = 1;
          fish4_turn_anim.animationSpeed = 0.5;         
          fish4_turn_anim.play();
        }
        
        
      var fish4_turn = [];
      
      for (var j = 1; j <= 24; j++){
        var value1 = j < 10 ? '0' + j : j;
        fish4_turn.push(PIXI.Texture.fromFrame('Fish4a_' + value1 + '.png'));
      }
      
      var fish4_turn_anim = new PIXI.extras.AnimatedSprite(fish4_turn);
      
        fish4_turn_anim.x = Math.floor(Math.random() * app.renderer.width);
        fish4_turn_anim.y = 200;        
        fish4_turn_anim.scale.set(0.15 + Math.random() * 0.2);
        fish4_turn_anim.pivot.set(0.5, 0.5);
        fish4_turn_anim.animationSpeed = 0.3;
        fish4_turn_anim.direction = Math.random() * Math.PI * 2;
        fish4_turn_anim.turningSpeed = Math.random() - 1;
        fish4_turn_anim.speed = 1 + Math.random() * 1.1;
        fish4_turn_anim.x += Math.sin(count) * fish4_turn_anim.animationSpeed;
        fish4_turn_anim.y += Math.cos(count) * fish4_turn_anim.animationSpeed;
        fish4_turn_anim.loop = false;
        fish4_turn_anim.visible = false;
        app.stage.addChild(fish4_turn_anim);
                
      var fish5_turn = [];
      
      for (var j = 1; j <= 24; j++){
        var value1 = j < 10 ? '0' + j : j;
        fish5_turn.push(PIXI.Texture.fromFrame('Fish5a_' + value1 + '.png'));
      }
      
      var fish5_turn_anim = new PIXI.extras.AnimatedSprite(fish5_turn);
      
        fish5_turn_anim.x = 1100;
        fish5_turn_anim.y = 250;        
        fish5_turn_anim.scale.set(0.15 + Math.random() * 0.2);
        fish5_turn_anim.pivot.set(0.5, 0.5);
        fish5_turn_anim.animationSpeed = 0.3;
        fish5_turn_anim.direction = Math.random() * Math.PI * 2;
        fish5_turn_anim.turningSpeed = Math.random() - 1;
        fish5_turn_anim.speed = 1 + Math.random() * 1.1;
        fish5_turn_anim.x += Math.sin(count) * fish5_turn_anim.animationSpeed;
        fish5_turn_anim.y += Math.cos(count) * fish5_turn_anim.animationSpeed;
        fish5_turn_anim.play();
        fish5_turn_anim.loop = false;
        app.stage.addChild(fish5_turn_anim);
        
        
      var fish5_turn2 = [];
      
      for (var j = 1; j <= 24; j++){
        var value2 = j < 10 ? '0' + j : j;
        fish5_turn2.push(PIXI.Texture.fromFrame('Fish5b_' + value2 + '.png'));
      }
      
      var fish5_turn2_anim = new PIXI.extras.AnimatedSprite(fish5_turn2);
      
        fish5_turn2_anim.x = 1250;
        fish5_turn2_anim.y = 550;        
        fish5_turn2_anim.scale.set(0.15 + Math.random() * 0.2);
        fish5_turn2_anim.pivot.set(0.5, 0.5);
        fish5_turn2_anim.animationSpeed = 0.3;
        fish5_turn2_anim.direction = Math.random() * Math.PI * 2;
        fish5_turn2_anim.turningSpeed = Math.random() - 1;
        fish5_turn2_anim.speed = 1 + Math.random() * 1.1;
        fish5_turn2_anim.x += Math.sin(count) * fish5_turn2_anim.animationSpeed;
        fish5_turn2_anim.y += Math.cos(count) * fish5_turn2_anim.animationSpeed;
        fish5_turn2_anim.play();
        fish5_turn2_anim.loop = false;
        fish5_turn2_anim.direction = Math.random() * Math.PI * 2;
        app.stage.addChild(fish5_turn2_anim);
  
           
      for (ii = 1; ii < 10; ii++) {

        
        var fish1 = [];

        for (var iii = 1; iii <= 24; iii++) {
            var val1 = iii < 10 ? '0' + iii : iii;

            fish1.push(PIXI.Texture.fromFrame('Fish1_' + val1 + '.png'));
        }
      
        var fish1_anim = new PIXI.extras.AnimatedSprite(fish1);        

        fish1_anim.x = Math.floor(Math.random() * app.renderer.width);
       // fish1_anim.y = 90 + Math.floor(Math.random() * app.renderer.height);
        fish1_anim.y = 90 + Math.floor(Math.random() + app.renderer.height  * 0.05);
        fish1_anim.scale.set(0.15 + Math.random() * 0.2);
        fish1_anim.pivot.set(0.5, 0.5);
        fish1_anim.animationSpeed = 0.45;
        //anim.direction += anim.turningSpeed * 0.01;
        fish1_anim.direction = Math.random() * Math.PI * 2;
        fish1_anim.turningSpeed = Math.random() - 1;
        fish1_anim.speed = 1.8 + Math.random() * 1.1;
        fish1_anim.x += Math.sin(count) * fish1_anim.animationSpeed;
        fish1_anim.y += Math.cos(count) * fish1_anim.animationSpeed;
        fish1_anim.play();
        app.stage.addChild(fish1_anim);
        
        fishArray.push(fish1_anim); 
                
        var fish2 = [];

        for (var i = 1; i <= 24; i++) {
            var val2 = i < 10 ? '0' + i : i;
            fish2.push(PIXI.Texture.fromFrame('Fish2_' + val2 + '.png'));
        }
        
        var fish2_anim = new PIXI.extras.AnimatedSprite(fish2);
        
        fish2_anim.x = Math.floor(Math.random() * app.renderer.width);
        //fish2_anim.y = 90 + Math.floor(Math.random() * app.renderer.height);
        fish2_anim.y = 90 + Math.floor(Math.random() + app.renderer.height  * 0.22);
        fish2_anim.scale.set(0.15 + Math.random() * 0.15);
        //fish2_anim.pivot.set(0.5, 0.5);
        fish2_anim.animationSpeed = 0.45;
        //fish2_anim.direction += fish2_anim.turningSpeed * 0.01;
        fish2_anim.direction = Math.random();
        fish2_anim.turningSpeed = Math.random() - 1;
        fish2_anim.speed = 1.8 + Math.random() * 1.1;
        fish2_anim.x +=  fish2_anim.animationSpeed;
        fish2_anim.y +=  fish2_anim.animationSpeed;
        fish2_anim.play();


        fishArray.push(fish2_anim);
        app.stage.addChild(fish2_anim);
        
        var fish3 = [];

        for (var i = 1; i <= 24; i++) {
            var val3 = i < 10 ? '0' + i : i;

            fish3.push(PIXI.Texture.fromFrame('Fish3_' + val3 + '.png'));
        }
        
        var fish3_anim = new PIXI.extras.AnimatedSprite(fish3);
        
        fish3_anim.x = Math.floor(Math.random() * app.renderer.width);
        fish3_anim.y = 90 + Math.floor(Math.random() + app.renderer.height  * 0.35);
        //fish3_anim.y = 90 + Math.floor(Math.random() * app.renderer.height);        
        fish3_anim.scale.set(0.15 + Math.random() * 0.15);
        //fish2_anim.pivot.set(0.5, 0.5);
        fish3_anim.animationSpeed = 0.45;
        //fish2_anim.direction += fish2_anim.turningSpeed * 0.01;
        fish3_anim.direction = Math.random();
        fish3_anim.turningSpeed = Math.random() - 1;
        fish3_anim.speed = 1.8 + Math.random() * 1.1;
        fish3_anim.x +=  fish3_anim.animationSpeed;
        fish3_anim.y +=  fish3_anim.animationSpeed;
        fish3_anim.play();


        fishArray.push(fish3_anim);
        app.stage.addChild(fish3_anim);
        
        var fish4 = [];

        for (var i = 1; i <= 24; i++) {
            var val4 = i < 10 ? '0' + i : i;

            fish4.push(PIXI.Texture.fromFrame('Fish4_' + val4 + '.png'));
        }
        
        var fish4_anim = new PIXI.extras.AnimatedSprite(fish4);
        
        fish4_anim.x = Math.floor(Math.random() * app.renderer.width);
        fish4_anim.y = 90 + Math.floor(Math.random() + app.renderer.height  * 0.45);
        //fish4_anim.y = 90 + Math.floor(Math.random() * app.renderer.height);        
        fish4_anim.scale.set(0.15 + Math.random() * 0.15);
        //fish2_anim.pivot.set(0.5, 0.5);
        fish4_anim.animationSpeed = 0.45;
        //fish2_anim.direction += fish2_anim.turningSpeed * 0.01;
        fish4_anim.direction = Math.random();
        fish4_anim.turningSpeed = Math.random() - 1;
        fish4_anim.speed = 1.8 + Math.random() * 1.1;
        fish4_anim.x +=  fish4_anim.animationSpeed;
        fish4_anim.y +=  fish4_anim.animationSpeed;
        fish4_anim.play();


        fishArray.push(fish4_anim);
        app.stage.addChild(fish4_anim);
              
        var fish5 = [];
        
        for (var i = 1; i <= 24; i++) {
            var val5 = i < 10 ? '0' + i : i;

            fish5.push(PIXI.Texture.fromFrame('Fish5_' + val5 + '.png'));
        }
                  
        var fish5_anim = new PIXI.extras.AnimatedSprite(fish5);        

        fish5_anim.x = Math.floor(Math.random() * app.renderer.width);
        fish5_anim.y = 90 + Math.floor(Math.random() + app.renderer.height  * 0.56);
       // fish5_anim.y = 90 + Math.floor(Math.random() * app.renderer.height);        
        fish5_anim.scale.set(0.18 + Math.random() * 0.14);
        fish5_anim.pivot.set(0.5, 0.5);
        fish5_anim.animationSpeed = 0.45;
        //anim.direction += anim.turningSpeed * 0.01;
        fish5_anim.direction = Math.random() * Math.PI * 2;
        fish5_anim.turningSpeed = Math.random() - 1;
        fish5_anim.speed = 1.8 + Math.random() * 1.1;
        fish5_anim.x += Math.sin(count) * fish5_anim.animationSpeed;
        fish5_anim.y += Math.cos(count) * fish5_anim.animationSpeed;
        fish5_anim.play();
        
        fishArray.push(fish5_anim);
        app.stage.addChild(fish5_anim);
          
}
      
   var animBoundsPadding = 100;

   var animBounds = new PIXI.Rectangle(
      -animBoundsPadding,
      -animBoundsPadding,
      app.renderer.width + animBoundsPadding * 2,
      app.renderer.height + animBoundsPadding * 2
   );
   
             
app.ticker.add(function(delta) {
  
  fish1_turn_anim.x -= 3 * delta;
  fish1_turn2_anim.x -= 3 * delta;
  fish2_turn_anim.x -=3 * delta;
  fish2_turn2_anim.x -= 3 * delta;
  fish3_turn_anim.x -=2 * delta;
  fish3_turn2_anim.x -= 1 * delta;
  fish4_turn_anim.x -= 3 * delta;
  //fish4_turn2_anim.x -= 2 * delta;
  fish5_turn_anim.x -= 3 * delta;
  fish5_turn2_anim.x -= 3 * delta;
  bubble_single.y -=2;
  //fish4_complete_anim.x += 2 * delta;
  
    algae_3_left.scale.x = 0.5 + Math.sin(count) * 0.1;
    algae_3_left.scale.y = 0.5 + Math.cos(count) * 0.05;
    algae_3_left.x = 200;
    algae_3_left.y = 690;
  
   
       // iterate through the fish and update the positions
    for (var i = 0; i < fishArray.length; i++) {
      

        var animatie = fishArray[i];
        animatie.direction += animatie.turningSpeed * 0.01;
        animatie.x += 1.5 * delta;
        animatie.x += Math.sin(animatie.direction) * animatie.speed;
        animatie.y += Math.cos(animatie.direction) * animatie.speed/3;
        
        // wrap the fish by testing their bounds...
        if (animatie.x < animBounds.x) {
            animatie.x += animBounds.width;
        }
        else if (animatie.x > animBounds.x + animBounds.width) {
            animatie.x -= animBounds.width;
        }

        if (animatie.y < animBounds.y) {
            animatie.y += animBounds.height;
        }
        else if (animatie.y > animBounds.y + animBounds.height) {
            animatie.y -= animBounds.height;
        }
    }
    
});
      
}

var container = new PIXI.Container();
container.x = app.renderer.width/2 ;
container.y = app.renderer.height/2;

var algae_1_left =  PIXI.Sprite.fromImage('images/algae_1_left.png');
algae_1_left.anchor.set(0.5);

var bubble_single = PIXI.Sprite.fromImage('images/bubble_single.png');
       
    bubble_single.visible = false;


var algae_1_right =  PIXI.Sprite.fromImage('images/algae_1_right.png');
algae_1_right.anchor.set(0.5);

var algae_2_primplan =  PIXI.Sprite.fromImage('images/algae_2_primplan.png');
algae_2_primplan.anchor.set(0.5);

var algae_2_left =  PIXI.Sprite.fromImage('images/algae_2_left.png');
algae_2_left.anchor.set(0.5);

var algae_2_right =  PIXI.Sprite.fromImage('images/algae_2_left.png');
algae_2_right.anchor.set(0.5);
       


container.addChild(algae_1_left,algae_1_right,algae_2_left,algae_2_right,algae_2_primplan,bubble_single);
app.stage.addChild(container);

var container2 = new PIXI.Container();
container2.x = app.renderer.width/10 ;
container2.y = app.renderer.height/2;

var fish1 =  PIXI.Sprite.fromImage('images/fish1.png');
fish1.anchor.set(0.5);

    fish1.scale.x = 0.2;
    fish1.scale.y = 0.2;
    
    container2.addChild(fish1);
    app.stage.addChild(container2);

var count = 0;

// Opt-in to interactivity
      algae_1_left.interactive = true;
      // Shows hand cursor
      algae_1_left.buttonMode = true;
      // Pointers normalize touch and mouse

      algae_1_left.on('pointerover', onClickBubble);

    function onClickBubble () {
      bubble_single.x = -600;
      bubble_single.y = -90;
      bubble_single.scale.x = bubble_single.scale.y = 0.05;
      bubble_single.visible = true;
    }

app.ticker.add(function() {

    fish1.x += 2.5;
    bubble_single.y -=2;
   
    algae_1_left.scale.x = 1 + Math.sin(count) * 0.05;
    algae_1_left.scale.y = 1 + Math.cos(count) * 0.09;
    algae_1_left.x = -550;
    algae_1_left.y = 100;
    
    algae_1_right.scale.x = 1 + Math.sin(count) * 0.05;
    algae_1_right.scale.y = 1 + Math.cos(count) * 0.05;
    algae_1_right.x = 550;
    algae_1_right.y = 100;
    
    algae_2_primplan.scale.x = 1 + Math.sin(count) * 0.1;
    //algae_2_primplan.scale.y = 1 + Math.cos(count) * 1;
    algae_2_primplan.x = 480;
    algae_2_primplan.y = 250;   
    
    algae_2_left.scale.x = 0.5 + Math.sin(count) * 0.1;
    algae_2_left.scale.y = 0.5 + Math.cos(count) * 0.05;
    algae_2_left.x = -300;
    algae_2_left.y = 300;
    algae_2_left.alpha = 1;
    
    algae_2_right.scale.x = - (1 + Math.sin(count) * 0.1);
    algae_2_right.scale.y =  (1 + Math.cos(count) * 0.05);
    algae_2_right.x = 350;
    algae_2_right.y = 300;
    algae_2_right.alpha = 1;

    count += 0.01;

});

    var horizontal_direction = "right";
       
        
    var my_fish = new PIXI.Sprite.fromImage('images/fish2.png');
    container2.addChild(my_fish);
    app.stage.addChild(container2);


    my_fish.position.set(100,app.renderer.height/16);
    //console.log("pozitie", my_fish.x);
		my_fish.anchor.set(0.5,0.5);
		my_fish.scale.set(0.2,0.2);
    
        //console.log("starting animation");
        state = idle;

        gameLoop();
         
        function gameLoop() {

		requestAnimationFrame(gameLoop);

		state();
        
		app.renderer.render(app.stage);

	}
    
    function idle() {
		if (horizontal_direction == "right") {
			if (my_fish.x < app.renderer.width) {
				my_fish.x += 5;
        //console.log("righttttt");
			} else {
				horizontal_direction = "left";
				my_fish.scale.set(-0.2,0.2);
        //console.log("leftttttttttt");
			}
		} else if (horizontal_direction == "left") {
			if (my_fish.x > 0) {
				my_fish.x -= 5;
        //console.log("left2");
			}
      else {
				my_fish.scale.set(0.2,0.2);
				horizontal_direction = "right";
        //console.log("right2");
			}
		}
    
	}

// Opt-in to interactivity
      my_fish.interactive = true;
      // Shows hand cursor
      my_fish.buttonMode = true;
      // Pointers normalize touch and mouse

      my_fish.on('pointerdown', onClick);

    function onClick () {
    my_fish.scale.x *= 1.25;
    my_fish.scale.y *= 1.25;
    my_fish.alpha -= 0.01;
    }

// Opt-in to interactivity
      fish1.interactive = true;
      // Shows hand cursor
      fish1.buttonMode = true;
      // Pointers normalize touch and mouse

      fish1.on('pointerdown', onClick1);

  function onClick1 () {
    fish1.scale.x = 0.20;
    fish1.scale.y = -0.20;
      
    dead_fish = 3;  
    for (var dead_fish = 1; dead_fish <= 3; dead_fish ++){
      app.ticker.add(function() {   
        fish1.y -= 2;
        //fish1.x = fish1.x;
        fish1.alpha -= 0.01;
      });
    }
  }